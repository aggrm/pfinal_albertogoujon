/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
/**
 *
 * @author Alberto Goujon
 */
public class GestorDOM {
    
    Document doc = null;                                                        //Para poder reutilizar los datos del documento en los métodos
    
    public int abrir_xml_dom(File fichero) 
    {
        try 
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();//Crea un objeto de DocumentBuilderFactory
            factory.setIgnoringComments(true);                                  //El modelo DOM no debe contemplar los comentarios que tengo el xml
            factory.setIgnoringElementContentWhitespace(true);                  //Ignora los espacios en blanco que tenga el documento
            DocumentBuilder builder = factory.newDocumentBuilder();             //Crea un objeto DocumentBuilder cargar en él le estructura de árbol DOM a partir del XLM seleccionado
            doc = builder.parse(fichero);                                       //Interpreta el documento XML y genera el DOM equivalente
            return 0;                                                           //Ahora doc al árbol DOM listo para ser recorrido    
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            return -1;
        }
    }

    public int addToDOM(String nombre, String fabricante, String modelo, String color, 
            String year_fabricacion, String precio,
            String id, String stock, String categoria, String seccion) 
    {
        try 
        {
            //--------------------------Elementos-------------------------------
            Node nNombre = doc.createElement("Nombre");                         //Se crea un nodo tipo Element con nombre ‘nombre’(<Nombre>)
            Node nNombre_text = doc.createTextNode(nombre);                     //Se crea un nodo tipo texto con el nombre del articulo
            nNombre.appendChild(nNombre_text);                                  //Se añade el nodo de texto con el nombre como hijo del elemento Nombre 

            //Se hace lo mismo que con título a Fabricante (<Fabricante>)
            Node nFabricante = doc.createElement("Fabricante");
            Node nFabricante_text = doc.createTextNode(fabricante);
            nFabricante.appendChild(nFabricante_text);

            //Se hace lo mismo que con título a modelo (<Modelo>)
            Node nModelo = doc.createElement("Num_modelo");
            Node nModelo_text = doc.createTextNode(modelo);
            nModelo.appendChild(nModelo_text);
            
            //Se hace lo mismo que con título a color (<Color>)
            Node nColor = doc.createElement("Color");
            Node nColor_text = doc.createTextNode(color);
            nColor.appendChild(nColor_text);
            
            //Se hace lo mismo que con título a año de fabricación (<Year_Fabricacion>)
            Node nYear_fabricacion = doc.createElement("Year_fabricacion");
            Node nYear_fabricacion_text = doc.createTextNode(year_fabricacion);
            nYear_fabricacion.appendChild(nYear_fabricacion_text);
            
            //Se hace lo mismo que con título a color (<Color>)
            Node nPrecio = doc.createElement("Precio");
            Node nPrecio_text = doc.createTextNode(precio);
            nPrecio.appendChild(nPrecio_text);
            //------------------------------------------------------------------
            
            //--------------------------Etiquetas-------------------------------
            //Se crea un nodo de tipo elemento (<Articulo>)
            Node narticulo = doc.createElement("Articulo");
            ((Element) narticulo).setAttribute("id_articulo", id);
            ((Element) narticulo).setAttribute("stock", stock);
            ((Element) narticulo).setAttribute("categoria", categoria);
            ((Element) narticulo).setAttribute("seccion", seccion);
            //------------------------------------------------------------------
            
            //Se añade a Aticulo todos sus elementos
            narticulo.appendChild(nNombre);
            narticulo.appendChild(nFabricante);
            narticulo.appendChild(nModelo);
            narticulo.appendChild(nColor);
            narticulo.appendChild(nYear_fabricacion);
            narticulo.appendChild(nPrecio);

            //Finalmente, se obtiene el primer nodo del documento y a él se le 
            //añade como hijo el nodo articulo que ya tiene colgando todos sus 
            //hijos y atributos creados antes.
            Node raiz = doc.getChildNodes().item(0);
            raiz.appendChild(narticulo);

            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
    
    public int guardaDomComoFile(File nombreArchivo) 
    {
        /*Cuando añadpo el articulo nuevo, al serializarlo me cambia el orden de los atributos, y al recorrer con sax es un problema. 
        Pero cuando realizas una consulta de las primeras, si se atualiza bien y se puede llegar a utilizar toda la aplicacción
        */
       
        try 
        {
            OutputFormat format = new OutputFormat(doc);                        //Especifico el formato de salida que es tipo OutputFormat
            format.setIndenting(true);                                          //Especifico que la salida esté indentada
            
            //Escribe el contenido en el archivo_xml con el formato correspondiente
            XMLSerializer serializer = new XMLSerializer(new FileOutputStream(nombreArchivo), format);
            serializer.serialize(doc);
            return 0;
        } 
        catch (Exception e) 
        {
            return -1;
        }
    }

}
