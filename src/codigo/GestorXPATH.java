/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;
/**
 *
 * @author Alberto Goujon
 */

public class GestorXPATH {
    
    Document doc = null;                                                        //Variable global para poder trabajar con el archivo 

    public int abrir_xml_con_dom(File fichero) {
        try {
            //Crea un objeto DocumentBuilderFactory para el DOM (JAXP)
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();//Crea un objeto de DocumentBuilderFactory
            factory.setIgnoringComments(true);                                  //El modelo DOM no debe contemplar los comentarios que tengo el xml
            factory.setIgnoringElementContentWhitespace(true);                  //Ignora los espacios en blanco que tenga el documento
            DocumentBuilder builder = factory.newDocumentBuilder();             //Crea un objeto DocumentBuilder cargar en él le estructura de árbol DOM a partir del XLM seleccionado
            doc = builder.parse(fichero);                                       //Interpreta el documento XML y genera el DOM equivalente
            return 0;                                                           //Ahora doc al árbol DOM listo para ser recorrido    
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }

    public String ejecutaXPath(String consulta) {
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();                //Crea el objeto XPath
            XPathExpression exp = xpath.compile(consulta);                      //Crea un XPathExpression con la consulta deseada

            Object result = exp.evaluate(doc, XPathConstants.NODESET);          //Ejecuta la consulta indicando que se ejecute sobre el DOM y que devolverá el resultado como una lista de nodos.
            NodeList nodeList = (NodeList) result;
            
            //Ahora recorrere la lista para sacar los resultados
            String salida = "";
            String datos_nodo[];
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node ntemp = nodeList.item(i);                                  //Para consulta de Libro
                if(ntemp.getNodeName() == "Articulo")
                {
                    datos_nodo = procesarArticulo(ntemp);                       //Obtengo la lista del metodo ya con todos los datos de ese (<Articulo>)
                    
                    //Es lo que voy a imprimir por el jTextArea
                    salida = salida + "\n" + "ID: " + datos_nodo[1];
                    salida = salida + "\n" + "Stock: " + datos_nodo[3];
                    salida = salida + "\n" + "Categoría: " + datos_nodo[0];
                    salida = salida + "\n" + "Sección: " + datos_nodo[2];
                    salida = salida + "\n" + "Nombre: " + datos_nodo[4];
                    salida = salida + "\n" + "Fabricante: " + datos_nodo[5];
                    salida = salida + "\n" + "Num_Modelo: " + datos_nodo[6];
                    salida = salida + "\n" + "Color: " + datos_nodo[7];
                    salida = salida + "\n" + "Año de fabricación: " + datos_nodo[8];
                    salida = salida + "\n" + "Precio: " + datos_nodo[9];
                    salida = salida + "\n -----------------------";
                }
                else
                {
                    salida = salida + "\n" 
                            + nodeList.item(i).getChildNodes().item(0).getNodeValue() + "\n";
                }
                
            }
            System.out.println(salida);

            return salida;
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
            return "No se pudo realizar la consulta";
        }
    }

    protected String[] procesarArticulo(Node n) {
        String datos[] = new String[10];                                        //Lista con la informacon de los articulos
        Node ntemp = null;                                                      //Nodos temporales
        int contador = 4;                                                       //Contador para poder obtener nombre, fabricante, modelo... 
                                                                                //se inicia en 4 porque los tres primeros puestos ya esta con la informacion de las etiquetas

        datos[0] = n.getAttributes().item(0).getNodeValue();                    //categoria
        datos[1] = n.getAttributes().item(1).getNodeValue();                    //id
        datos[2] = n.getAttributes().item(2).getNodeValue();                    //sección
        datos[3] = n.getAttributes().item(3).getNodeValue();                    //stock
        
        NodeList nodos = n.getChildNodes();                                     //Obtiene los hijos del Artículo (nombre, fabricante, num_modelo...)

        for (int i = 0; i < nodos.getLength(); i++) {
            ntemp = nodos.item(i);                                              //nos encontramos en la etiqueta (<Articulo>)
            if (ntemp.getNodeType() == Node.ELEMENT_NODE) {
                datos[contador] = ntemp.getFirstChild().getNodeValue();         //Acedo al nodo tipo texto que es el que contiene el valor del nombre del articulo
                contador++;
            }
        }
        return datos;
    }
}
