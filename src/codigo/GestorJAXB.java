/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;


import java.util.List;
import java.io.File;
import java.util.Arrays;
import javaArticulos.Tienda;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
/**
 *
 * @author Alberto Goujon
 */
public class GestorJAXB {
    
    Tienda misArticulos;
    
    public int abrir_XML_JAXB(File fichero) 
    {
        JAXBContext contexto;
        try 
        {
            contexto = JAXBContext.newInstance(Tienda.class);                   //Crea una instancia JAXB
            Unmarshaller u = contexto.createUnmarshaller();                     //Crea un objeto Unmarsheller
            misArticulos = (Tienda) u.unmarshal(fichero);                       //Deserializa (unmarshal) el fichero
            return 0;
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            return -1;
        }
    }
    
    public int guardar_XML_JAXB(File fichero)
    {
        JAXBContext contexto;
        try 
        {
            contexto = JAXBContext.newInstance(Tienda.class);                   //Crea una instancia JAXB
            Marshaller m = contexto.createMarshaller();                         //Crea un objeto Marsheller
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);              //Dar forma al fichero
            m.marshal(misArticulos, fichero);                                   //Serializa (marshal) el fichero
            return 0;
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            return 1;
        }
    }
    
    public String[] buscaNodo(String valorABuscar)
    {
        String datos[]= new String[10];
        List<Tienda.Articulo> listArticulo = misArticulos.getArticulo();        //Lista del nodo del articulo ha buscar
        
        try 
        {   
            for (int i=0; i<listArticulo.size(); i++)
            {
                if(listArticulo.get(i).getIdArticulo().equals(valorABuscar))
                {
                    datos[0] = listArticulo.get(i).getIdArticulo();
                    datos[1] = listArticulo.get(i).getStock();
                    datos[2] = listArticulo.get(i).getCategoria();
                    datos[3] = listArticulo.get(i).getSeccion();
                    datos[4] = listArticulo.get(i).getNombre();
                    datos[5] = listArticulo.get(i).getFabricante();
                    datos[6] = listArticulo.get(i).getNumModelo();
                    datos[7] = listArticulo.get(i).getColor();
                    datos[8] = listArticulo.get(i).getYearFabricacion();
                    datos[9] = listArticulo.get(i).getPrecio();
                }
            } 
            System.out.println(Arrays.toString(datos));
            return datos;
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return datos;
        }
    }
    public void modificaNodo (String valorABuscar, String nuevostock, String nuevacategoria, 
            String nuevaseccion, String nuevonombe, String nuevoFabricante,String nuevoModelo, 
            String nuevoColor, String nuevoYearFabricacion,String nuevoPrecio)
    {
          List<Tienda.Articulo> listArticulo = misArticulos.getArticulo();      //Lista del nodo a modificar
          
        for (int i=0; i<listArticulo.size(); i++)
        {
            if(listArticulo.get(i).getIdArticulo().equals(valorABuscar))
            {
                listArticulo.get(i).setIdArticulo(valorABuscar);
                listArticulo.get(i).setStock(nuevostock);
                listArticulo.get(i).setCategoria(nuevacategoria);
                listArticulo.get(i).setSeccion(nuevaseccion);
                listArticulo.get(i).setNombre(nuevonombe);
                listArticulo.get(i).setFabricante(nuevoFabricante);
                listArticulo.get(i).setNumModelo(nuevoModelo);
                listArticulo.get(i).setColor(nuevoColor);
                listArticulo.get(i).setYearFabricacion(nuevoYearFabricacion);
                listArticulo.get(i).setPrecio(nuevoPrecio);
            
            }
        }
          
    }
    
    
}
