/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Alberto Goujon
 */
public class GestorSAX {
    
    SAXParser parser;
    ManejadorSAX sh;
    File ficheroXML;

    public int abrir_XML_SAX(File fichero) 
    {
        try 
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();          //Nueva instancia de SAXParserFactory
            parser = factory.newSAXParser();                                    //Se crea un objeto SAXParser para interpretar el documento XML

            sh = new ManejadorSAX();                                            //Se crea una instancia del manejador que sera el que recorra el documento XML secuencialmente
            ficheroXML = fichero;                       
            return 0;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            return -1;
        }
    }
    
    public String recorrerSAX ()
    {
        try 
        {
            sh.cadena_resultado="";
            parser.parse(ficheroXML, sh);
            return  sh.cadena_resultado;
        }
        catch(SAXException e) 
        {
            e.printStackTrace();
            return  "Carga un fichero válido";
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return  "Carga un fichero válido";
        }
    }
}

class ManejadorSAX extends DefaultHandler 
{
    int ultimoElement;
    String cadena_resultado = "";

    public ManejadorSAX() {
        ultimoElement = 0;
    }
    
    //Se sobrecarga (redefine) el método startElement
    @Override public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException 
    {
        //-------------------------Inicio Etiquetas-----------------------------
        //Si encuentra la etiqueta <Articulo> añade a la cadena_resultado los datos de id_artículo
        if (qName.equals("Articulo")) 
        {
            cadena_resultado = cadena_resultado + "\nID: " + atts.getValue(atts.getQName(0))+ "\n";
            cadena_resultado = cadena_resultado + "\nStock: " + atts.getValue(atts.getQName(1))+ "\n";
            cadena_resultado = cadena_resultado + "\nCategoria: " + atts.getValue(atts.getQName(2))+ "\n";
            cadena_resultado = cadena_resultado + "\nSección: " + atts.getValue(atts.getQName(3))+ "\n";
            ultimoElement = 1;
        }
        //----------------------------------------------------------------------
        
        //-------------------------Inicio Elementos-----------------------------
        //Si encuentra la etiqueta <Nombre> añade a la cadena_resultado el título
        else if (qName.equals("Nombre")) 
        {
            ultimoElement = 2;
            cadena_resultado = cadena_resultado + "\nEl nombre es: ";
        }
        
        //Si encuentra la etiqueta <Fabricante> añade a la cadena_resultado el autor
        else if (qName.equals("Fabricante")) 
        {
            ultimoElement = 3;
            cadena_resultado = cadena_resultado + "\nEl fabricante es: ";
        }
        
        //Si encuentra la etiqueta <Num_modelo> añade a la cadena_resultado la editorial
        else if (qName.equals("Num_modelo")) 
        {
            ultimoElement = 4;
            cadena_resultado = cadena_resultado + "\nEl número de modelo es: ";
        }
        else if (qName.equals("Color")) 
        {
            ultimoElement = 5;
            cadena_resultado = cadena_resultado + "\nEl color es: ";
        }
        else if (qName.equals("Year_fabricacion")) 
        {
            ultimoElement = 6;
            cadena_resultado = cadena_resultado + "\nEl año de fabricación es: ";
        }
        else if (qName.equals("Precio")) 
        {
            ultimoElement = 7;
            cadena_resultado = cadena_resultado + "\nEl precio es: ";
        }
        //----------------------------------------------------------------------
    }

    @Override public void endElement(String uri, String localName, String qName) throws SAXException 
    {
        //Cuando sale del ultimoElement <Articulo>, se pone una línea discontinua en la salida.
        if (qName.equals("Articulo")) 
        {
            System.out.println("He encontrado el final de un elemento.");
            cadena_resultado = cadena_resultado + "\n --------------------";
        }
    }

    @Override public void characters(char[] ch, int start, int length) throws SAXException 
    {
        
        for (int i = start; i < length + start; i++) 
        {
            if (ultimoElement == 2 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 3 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 4 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 5 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 6 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 7 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
        }
    }
}

